package com.example.multiscreenapp

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import android.util.Log

class FourthActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fourth_activity)

        val goForwardBtn = findViewById<Button>(R.id.forwardBtn)
        val goBackBtn = findViewById<Button>(R.id.backBtn)

        goForwardBtn.setOnClickListener {
            val firstIntent = Intent(this, FirstActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(firstIntent)
        }

        goBackBtn.setOnClickListener {
            finish()
        }
    }

}