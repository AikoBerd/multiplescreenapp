package com.example.multiscreenapp

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import android.util.Log

class SecondActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.second_activity)

        val goForwardBtn = findViewById<Button>(R.id.forwardBtn)
        val goBackBtn = findViewById<Button>(R.id.backBtn)

        goForwardBtn.setOnClickListener {
            val thirdIntent = Intent(this, ThirdActivity::class.java)
            startActivity(thirdIntent)
        }

        goBackBtn.setOnClickListener {
            finish()
        }
    }
}